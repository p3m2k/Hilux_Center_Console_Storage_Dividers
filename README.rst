#####################################
Hilux Center Console Storage Dividers
#####################################


Introduction
============

This repo contains design files for a set of 3D-printable dividers sized
for the center console storage compartment of 1984-89 Toyota pickups
(Hilux) and 4Runners (Hilux Surf).

The dividers are designed with Solvespace_, a free (GPLv3) parametric 3D
CAD tool.  STL files generated from the Solvespace source can be found
in the ``/build`` directory.

.. _SolveSpace: http://solvespace.com/

.. figure:: doc/images/assembly.png
   :figwidth: 50%
   :width: 100%
   :alt: Isometric CAD view of dividers.

   Isometric CAD view of dividers.

.. figure:: doc/images/dividers_installed.jpeg
   :figwidth: 50%
   :width: 100%
   :alt: Photo of dividers installed in center console storage.

   Dividers, installed in center console storage.



Future Plans
============

- Increase the number of available locations for lateral dividers (i.e.,
  more slots for more flexibility).

- Re-design in FreeCAD_.

- Design a version suitable for fabrication out of wood on a CNC router.

.. _FreeCAD: https://www.freecad.org/



Copying
=======

Everything here is copyright © 2019-2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
