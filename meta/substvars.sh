# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="Hilux Center Console Storage Dividers"
SLUG="hilux_center_console_storage_dividers"
DESCRIPTION="3D-printable dividers for the center console storage compartment of a 1984-89 Toyota pickups (Hilux) and 4Runners (Hilux Surf)."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2019-2023"
RELEASE_VERSION="1.0.0"
RELEASE_DATE="2023-01-09"
